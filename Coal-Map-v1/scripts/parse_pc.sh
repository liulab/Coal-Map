#Copyright (C) 2015 Hussein Hejase

#Coal-Map is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

#This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

#You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.


#!/bin/bash
numGeneTrees=$1
sed -i -e 1,6d data/EIGS/example.pca
sed -i -e 1,6d data/EIGS/example_global.pca

for j in `seq 1 $numGeneTrees`;
do
	sed -i -e 1,6d data/EIGS/example_$j.txt
	paste data/EIGS/example_$j.txt data/EIGS/example_global.pca > data/EIGS/example_combined_global_$j.txt
done
