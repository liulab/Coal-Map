#Copyright (C) 2015 Hussein Hejase

#Coal-Map is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

#This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

#You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/perl

$ENV{'PATH'} = "/mnt/home/hijazihu/EIG6.0.1/bin:$ENV{'PATH'}"; 
# MUST put smartpca bin directory in path for smartpca.perl to work

my $numGeneTrees=$ARGV[0];

#get global fixed effect
$command = "smartpca.perl";
$command .= " -i data/EIGS/geno.txt ";
$command .= " -a data/EIGS/snp.txt ";
$command .= " -b data/EIGS/ind.txt " ;
$command .= " -k 5 ";
$command .= " -q YES ";
$command .= " -o data/EIGS/example_global.pca ";
$command .= " -p data/EIGS/example_global.plot ";
$command .= " -e data/EIGS/example_global.eval ";
$command .= " -l data/EIGS/example_global.log ";
$command .= " -m 5 ";
$command .= " -t 2 ";
$command .= " -s 6.0 ";
$command .= " -q YES ";
print("$command\n");
system("$command");

for (my $g=1; $g <= $numGeneTrees; $g++) {
	#get local fixed effect
	$command = "smartpca.perl";
	$command .= " -i data/EIGS/geno_" . "$g.txt ";
	$command .= " -a data/EIGS/snp_" . "$g.txt ";
	$command .= " -b data/EIGS/ind.txt ";
	$command .= " -k 5 ";
	$command .= " -q YES ";
	$command .= " -o data/EIGS/example" . "_$g.txt ";
	$command .= " -p data/EIGS/example" . "_$g.txt ";
	$command .= " -e data/EIGS/example" . "_$g.txt ";
	$command .= " -l data/EIGS/example" . "_$g.txt ";
	$command .= " -m 5 ";
	$command .= " -t 2 ";
	$command .= " -s 6.0 ";
	print("$command\n");
	system("$command");
}
